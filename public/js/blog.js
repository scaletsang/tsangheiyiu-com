let $masonry;
let currentBlogId;
let sb_filter = "*"; //searchbox filter

window.onload = function() {
  setMasonryLayout();
  setupEventListeners();
  //open the blog if a valid hash is specified at the url
  if (window.location.hash !== null && window.location.hash !== '') {
    let id = decodeURI(window.location.hash);
    if ($(id).length) {
      openBlogByClick(false, id.slice(1));
    }
  }
}

//==================Masonry Layout ===============================

function setMasonryLayout() {
  $masonry = $("#multiple-blog-container").isotope({
    itemSelector: ".blogpost",
    layoutMode: 'masonry',
    masonry: {
      columnWidth: 20,
      horizontalOrder: true
    },
    getSortData: {
      date: function(elem) {dateToNum(elem);}
    },
    sortBy: "date"
  });
}

function dateToNum(elem) {
  let date_text = $(elem).find(".blog-date").text();

  //set blog day, month and year
  [_, day, monthInAlphabet, year] = /(\d+)\s(\w+)\s(\d+)/.exec(date_text);

  if (day.toString().length == 1) {
    day = "0" + day.toString();
  }

  //convert blog month into numbers so it is easier to sort
  let monthLs = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
  month = monthLs.indexOf(monthInAlphabet.toLowerCase()) + 1;

  return parseInt(year.toString() + month.toString() + day.toString());
}

//==================event listeners===============================

function setupEventListeners() {

  //open particular blogs when hash is changed
  $(window).on('hashchange', function(e) {
    let id = decodeURI(window.location.hash);
    if ($(id).length) {
      openBlogByClick(false, id.slice(1));
    }
  });

  //view posts
  $(".blogpost").click(function(elem) {
    openBlogByClick(true, elem);
  });

  //close button
  $("#close-button").click(function(){
    //remove URL hash
    history.replaceState(null, null, ' ');
    //update button
    $("#span-button").show();
    $("#retract-button").hide();
    //change view
    $("#single-blog-container").css("display", "none");
    $("#multiple-blog-container").show();
    $("#multiple-blog-container").css("width", "80vw");
    //refresh masonry layout
    $masonry.isotope({ filter: sb_filter });
    $masonry.isotope('layout');
  });

  //span button
  $("#span-button").click(function(){
    //update button
    $("#span-button").hide();
    $("#retract-button").show();
    //change view
    $("#multiple-blog-container").css("display", "none");
    $("#single-blog-container").css("width", "100%");
    $("#single-blog-container .blog-content").css({"padding": "0px 10% 0px 10%", "margin-bottom": "40px"});
    //refresh masonry layout
    $masonry.isotope({ filter: sb_filter });
    $masonry.isotope('layout');
  });

  //retract button
  $("#retract-button").click(function(elem){
    //update button
    $("#span-button").show();
    $("#retract-button").hide();
    //change view
    $("#multiple-blog-container").show();
    $("#single-blog-container").css("width", "50%");

    //filter out current blog from masonry layout
    let filterA = modifyStringList(sb_filter, function(item) {return `${item}:not(#${currentBlogId})`});
    $masonry.isotope({ filter: filterA });
    //$masonry.isotope({ filter: ":not(" + currentBlogId + ")" });
    //refresh masonry layout
    $masonry.isotope('layout');
  });

  //search box
  $("#search-box input").change(function(elem){
    let input = $("#search-box input").val();

    if (input == "") {
      sb_filter = "*"
      $masonry.isotope({ filter: sb_filter });
    } else {
      sb_filter = modifyStringList(input, function(item){
        return "." + item
      })

      //isotope filter
      $masonry.isotope({ filter: sb_filter });
    }
  })
}

function modifyStringList(string, f) {
  let list = string.split(", ");
  return list.map(f).join(", ");
}

function openBlogByClick(click, blog) {
  //get innerHTML and set currentBlogId
  if (click) {
    document.getElementById("single-blog-content").innerHTML = blog.currentTarget.innerHTML;
    currentBlogId = blog.currentTarget.id;
  } else {
    document.getElementById("single-blog-content").innerHTML = $(`#${blog}`).html();
    currentBlogId = blog;
  }
    
  //set URL hash
  window.location.hash = currentBlogId;

  //show single blog container and its content
  $("#single-blog-container").show();
  $("#single-blog-container .blog-detail-content").show();
  $("#single-blog-container .blog-content").css("padding", "0px 20px 20px 20px");

  //responsive design
  if ($(document).width() <= 796) {
    //adjust single and mutiple blog container widths
    $("#single-blog-container").css("width", "70%");
    $("#multiple-blog-container").css("display", "none");
    $("#span-button").hide();
  } else {
    //adjust single and mutiple blog container widths
    $("#single-blog-container").css("width", "50%");
    $("#multiple-blog-container").css("width", "50%");

    //filter out current blog from masonry layout
    let filterA = modifyStringList(sb_filter, function(item) {return `${item}:not(#${currentBlogId})`});

    $masonry.isotope({ filter: filterA });
    //$masonry.isotope({ filter: ":not(" + currentBlogId + ")" });
    //refresh masonry layout
    $masonry.isotope('layout');
  }

  //move the blog-tags div up to the top
  $("#single-blog-container .blog-buttons .blog-tags").remove();
  $("#single-blog-content .blog-tags").detach().appendTo("#single-blog-container .blog-buttons");

  //hide the abstract paragraph at the top
  $("#single-blog-content .blog-abstract-paragraph").hide();
  
  //load pdf when shown
  $("#single-blog-content .unloadedPDF").after(function(){
    let pdfUrl = this.getAttribute("pdfsrc");
    return `<object class="pdf" data="${pdfUrl}" type="application/pdf" width="100%" height="100%"><p>This browser does not support PDFs. Please download the PDF to view it:<a href="${pdfUrl}">Download PDF</a></p></object>`;
  });
  //load iframes when shown
  $("#single-blog-content .unloadedIframe").after(function(){
    return this.getAttribute("domelem");
  });
}