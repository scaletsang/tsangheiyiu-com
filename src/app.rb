require 'sinatra'
require 'rack/ssl-enforcer'
require 'haml'
require_relative 'blogpage.rb'

class App < Sinatra::Base
  set :server, "thin"
  set :public_folder, 'public'
  set :views, 'views'

  get '/' do
    redirect "/projects"
  end

  get '/home' do
    haml(:template, :locals => {
      :page_name => "z Z ZZZZZZ",
      :page_description => "I am going to sleep for now. only the blog is working. Spent days on building this site, tired.",
      :content => "<p>still building</p>"})
  end

  get '/event' do
    p = Blogpage.new
    haml(:template, :locals => {
      :page_name => "Participatory Performance",
      :page_description => "Please book a time with me, and I'm sure we will have a great time together in the exhibition space! Every day there will be at maximum 7 sessions, and together we will chat and enjoy the fun of making choice!",
      :content => p.render_blogpage("eventpage")})
  end

  get '/about' do
    p = Blogpage.new 
    haml(:template, :locals => {
      :page_name => "About",
      :page_description => "I am Anthony Tsang, my casual name. Tsang Hei Yiu (曾熙堯) is my Chinese name where the name itself contained much more than you think. ",
      :content => p.render_blogpage("aboutpage")})
  end

  get '/projects' do
    p = Blogpage.new "projects"
    haml(:template, :locals => {
      :page_name => "Projects",
      :page_description => "This page is an archive of old projects and an invitation to you if you are interested in my new in-progress projects.",
      :content => p.render_blogpage})
  end

  get '/blog' do
    p = Blogpage.new
    haml(:template, :locals => {
      :page_name => "Blog",
      :page_description => "This is a blog that records the process of my last year at my Bachelor's project (Oct 2020 - May 2021).
      This is my library of thoughts and ideas, including the organized, the forgotten, the incompleted and the flawed.",
      :content => p.render_blogpage})
  end

  get '/writing' do
    p = Blogpage.new "writing"
    haml(:template, :locals => {
      :page_name => "Writing",
      :page_description => "This page is a personal archive of writings that are more emotional, articulate in some ways. They are the crystals that build up my biggest project, myself.",
      :content => p.render_blogpage})
  end

end