# - * - coding: UTF-8 - * -
require 'kramdown'

class Post

  attr_accessor :id, :cover_img, :title, :date, :connections, :abstract, :tags, :md_content

  # take a blog in markdown format, set the blog's properties
  def initialize post
    #cover-img
    cover_img = post.match(/\[cover\-img\]\((.+)\)/) || ""
    @cover_img = cover_img[1]
    #title
    @title = post.match(/\#(.+)/)[1]
    #date
    @date = post.match(/\[date\](.+)/)[1]
    #id
    @id = "#{@title}#{@date}".strip.gsub(/[^\p{Word}]/, "").gsub(" ", "-")
    #connections
    @connections = post.match(/\[connections\](.+)/)[1]
    #abstract
    abstract = post.match(/\[abstract\]\s(.+)/) || ""
    @abstract = abstract[1]
    #tags
    tags_string = post.match(/\[tags\](.+)/)[1]
    @tags = tags_to_list tags_string
    #md_content
    @md_content = post
  end

  #change the md_content so that it can be parsed to kramdown md to html parser, returning html of a blog
  def render
    #cover-img
    if (/\[cover\-img\]\((.+)\)/).match? @md_content
      @md_content.sub! /\[cover\-img\]\((.+)\)/, ""
    end
    #title
    @md_content.sub! /\#(.+)/, ""
    #date
    @md_content.sub! /\[date\](.+)/, ""
    #connections
    @md_content.sub! /\[connections\](.+)/, ""
    #abstract
    @md_content.sub!(/\[abstract\](.+)/) {|n| "<p>#{$1}</p>"}
    #tags
    @md_content.sub! /\[tags\](.+)/, ""
    #presentation
    @md_content.gsub!(/\[presentation\]\((.+)\)/) {|n| "<div class='blog-presentation'>" + generate_presentation("#{$1}") + " </div>"}
    #pdf
    @md_content.gsub!(/\[pdf\]\((.+)\)/) {|n| "<div class='unloadedPDF' style='display:none;' pdfsrc='#{$1}'></div>"}
    #handle iframe
    @md_content.gsub!(/(<iframe.+>.*<\/iframe>)/) {|n| "<div class='unloadedIframe' style='display:none;' domelem='#{$1}'></div>"}

    return Kramdown::Document.new(@md_content).to_html
  end


  #custom markdown element, generate html structure for presentation markdown custom element
  def generate_presentation dir_path

    #return a list of img in the slides directory
    path = File.join("**", dir_path[1..-1], "slide*")
    img_pathlist = Dir.glob path      
    #sort the slides by their numbers
    sorted_img_pathlist = img_pathlist.sort_by { |img| img.scan(/\d+/).first.to_i }
    #filter only the list of slides, return a html of the slides
    slides_html = sorted_img_pathlist.reduce("") do |slides, img|
        slides + "<img alt='slide' src='" + img.sub(/public/, "") + "'>"
      end
    return slides_html
  end

  #custom markdown element, converts string into multiple html elemnts
  def tags_to_list tags
    #for connections md element
    tags.gsub!(/\s+/, "")
    return tags == "none" ? [] : tags.split(",")
  end

end
