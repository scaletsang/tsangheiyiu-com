require 'haml'
require_relative 'post.rb'

class Blogpage

  attr_accessor :post_paths, :post_htmls

  def initialize post_dirs="blogs"
    #return a list of markdown blogposts path
    @post_paths = Dir.glob("**", base: post_dirs).each { |bp| bp.prepend("./#{post_dirs}/")}
    @post_htmls = ""

  end

  #render all blogposts as html and return it as a string
  def render_post
    post_path_ls = @post_paths.map do |post_path|
      content = File.read post_path
      post = Post.new content

      #render the html of every post using haml
      post_haml = Haml::Engine.new File.read('views/post.haml')
      post_haml.render(Object.new, {
        :blog_id => post.id,
        :blog_cover_img => post.cover_img,
        :blog_title => post.title,
        :blog_date => post.date,
        :blog_connections => post.connections,
        :blog_abstract => post.abstract,
        :blog_tags => post.tags,
        :blog_content => post.render
        })

    end

    @post_htmls = post_path_ls.join
  end


  #render the whole blog page
  def render_blogpage haml="blogpage"
    render_post if haml == "blogpage"
    #render blogpage as html using haml
    blogpage = Haml::Engine.new File.read("views/#{haml}.haml")
    return blogpage.render(Object.new, {:blogposts => @post_htmls})

  end

end
