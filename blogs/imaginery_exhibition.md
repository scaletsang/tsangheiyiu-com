# An Imaginery exhibition

[cover-img](/media/imaginery_exhibition/imaginary_exhibition2.jpg)
[date] 24 Jun 2020
[tags] 
[connections] none

[abstract] What is an exhibition? It is not a place for presentation, it is a place for things to be curated. And through curation, all the great art pieces are killed and reincarnated into a new baby. It is believed that if you have done something great in your past life, you would reincarnated with inheritance of those good trace you had in your past life. A great artwork, even though killed by curation, would inevitably reincarnated with the aura of its greatness.

![img](/media/imaginery_exhibition/imaginary_exhibition1.jpg)
![img](/media/imaginery_exhibition/imaginary_exhibition2.jpg)
![img](/media/imaginery_exhibition/imaginary_exhibition3.jpg)
![img](/media/imaginery_exhibition/imaginary_exhibition4.jpg)
![img](/media/imaginery_exhibition/imaginary_exhibition5.jpg)
![img](/media/imaginery_exhibition/imaginary_exhibition6.jpg)

[Click here to download this proposal](/media/pdf/imaginery_exhibition.pdf)