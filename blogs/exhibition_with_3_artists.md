# An imaginary exhibition with 3 favorite artists

[date] 15 Oct 2020 afternoon
[tags] PC
[connections] none

The score:
Have a show with 3 artists you admire
identify where you fit in
Position yourself accordingly

Hayao Miysazaki

何倩彤 Hoo Sin Tung
[link](https://www.hooosintung.com)

謝德慶 Hsieh Tehching
<iframe src="https://www.youtube.com/embed/kijqJlOzSdI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
