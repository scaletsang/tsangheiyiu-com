# Reflection upon walking on a leg extension

[date] 14 May 2019
[tags] 
[connections] none

This is the documentation of my involvement in Olivier's project *L'Adapté*, where I was the one who tried to learn to walk on this absurd leg extension:
<iframe src="https://www.youtube.com/embed/o0oAnGFyz9I" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Portal to Olivier's project: [link](https://www.olivierbrandt.com/l-adapte)

[abstract] After about an hour or more of getting used to the leg extension, my body started to coordinate and walk. To coordinate, it means the following:

Making every step bears a risk. Without putting most weight on one leg and two hands extension, I could not have lifted the other leg and step forward. However, since the leg extension has to tendency to fall to the right and left side, putting weight on one leg is never a stable position. Hence, I need to incorporate two hands to balance for around 2 seconds. If I lost balance in that 2 seconds, I fell. However, there is a trick. If I continue to walk without stopping, it is easier to balance. Just like riding a bicycle at high speed is easier than riding as slow as a turtle.

Then, it is about the weight. Because the leg extension only connects to my calf, basically I am kneeling on the extension. It is quite impossible to lift it with only my calf. Therefore, the sole was needed to lift the back of the leg extension, while two hands extension as a joint to share the weight.

Finally, it is the angle between two legs. If you notice carefully, a human's two feet can never walk in parallel like a robot. We tend to make a certain angle in between both feet. It is an insignificant thing to notice when walking normally. Yet, walking on this leg extension, such angle became a crucial part of whether I can make a smooth transition to the next step. Little bumps on the ground or tiredness of my body sometimes would produce undesirable angles. Hence, a lot of the time I have to deal with these undesirable angles by redistributing force upon the 4 spots (two hand extension and two leg extension).

I would consider the above as a trace of where I put my attention, rather than a complaint about the inhumane design of the machine.

When everything is enlarged, the slightest details matters, just like why quantum physics is important to the exploration of the universe. When every little details matter, our automatic physical response, and our perceptive categorization does not work. I could not rely on how I usually walked nor how I normally classified dangerous scenarios. Everything has to be unlearned and relearned again. I went retrospect in time and became a baby than trying to learn how to walk.

As I started to walk while imagining how should I walk, for the first time, I experienced a loss of control upon my body. There is so much wiring that has been done when we were a child to allow our brain to control our movement smoothly.

After hours of walking on it (well I was in a zone, which means highly focused, not even able to talk, that is why my sense of time probably is distorted), my feet and my hips began to experience fatigue. My hands were comparatively less fatigued. I think this is funny because it might be how the elderly are experiencing when they have to walk on walking sticks.

[Click here to download this writing](/media/all_writings/Reflection_upon_walking_on_a_leg_extension.pdf)