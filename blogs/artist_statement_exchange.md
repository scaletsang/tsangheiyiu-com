# Artist statement exchange with Hoi Man

[date] 20 Oct 2020 evening
[tags] PC
[connections] none

The score:
You will exchange your statements.
You will write your partner a new statement that is one sentence long.
You will write a statement which is one word for the other person.
Think about what is being communicated in the work.

Hoi Man's artist statement:

> "Hoi Man’s work explores the moral struggles of the mortals when confronting the power. This power often appears as God in his work, however, he does not seek for religious reference when creating. In his work, God represents not only God itself but also the odds one might have to go against in his life.
>
> His photographs depict the moral anxiety as well as the solitude of his protagonists by a poetic and timeless aesthetic. His narrative dissolves the boundaries between literature, theatre and photography, the unique narrative is informed by his literary research and theatre studies. His pictorial visual language and theatrical composition allow him to convey different stories within a unified framework."

I turned it into one sentence: "My job is uncompressing our fear to be punished by the power, by the cosmos."
Firstly I thought that what he meant is confrontation to an endless power, has to do with struggles and anxiety. So I wrote "Hoi Man depicts our anxiety of being forced to confront the mess caused by the power, the god and the cosmos." But then I found that "moral anxiety" is a term from Freud that raised about the guilt, the self punishment when we fear we will be punished by the system, by the power because of something perhaps even unrelated that we have done. So I wrote "Hoi Man expresses the internal fear to be criticized when trying to confront the powerful." And then put it in first person speech: "My job is uncompressing our fear to be punished by the power, by the cosmos."

I concise this whole idea into one word, well kind of: "Humpty-Dumpty".
I think Humpty Dumpty represents really nicely our fear to be punished before we are even try to confront the power. He is afraid of falling off the wall, and crack. If you were an egg, you crack anyway regardless of the wall, the power. But the wall enhance your fear, and adding your chance to crack yourself. I think I added a lot of interpretation here but you get my point.

## My artist statement

As of my artist statement, Hoi Man put it into one sentence and the one word:

> Curation itself is art. I am a composer not of music but of objects that surrounds me, be it a bunch of trash, or be it a group of artworks my friends make. My primary way of working is the simple act of looking at the relationship between seemingly unrelated shapes. With all these chaos in mind, I try to sculpt them so that their shapes fit each other. When the shapes are visible, I am a paper sculptor who build architectures without glue. When the shapes are immaterial, I am a thinker who propose new theories by linking ideas from chinese, indian and western philosophy. But when the shapes get more and more abstract, Iike programming different software and codes together to build installation, I lost my own connection with people. I am for a curation that not only itself is working, but it also fits with my energy and the energy of the people around.
>
> "I search for the every connection possible."
>
> "Connection"

I think he concluded nicely, and at the same time revealed flaws in my thinking. I would be very much liking Hoi Man's interpretation if you asked me 6 months ago. The idea for searching for every connection **POSSIBLE** has exhausted me completely last semester. Looking for every possible connections is actually god's job, human is incapable of that. Playing and connecting ideas are fun but too much thinking in my own world, I lost the human aspect in my career. My interest, like improvisation, giving advices, listening to people, all involves other human or it involves me to find my connection with my own body. To improvise, you need to be in sync with your body; to dance, more about it; to advice and to listen, using some of my energy to spark the other's imagination. But I was building complicated installations, abstract structures, outdated with my interest. I lost my focus.

[abstract] So while this artist statement is recently written and it does speak about my practice, it only speak about the appearance of my practice. I think my core should be working with people, not in quantity, but in quality. In other words, to communicate with sensitivity, with care, with attentive listening, even if the communication happen really slowly or even durational. Let me try to write a new artist statement in another time.
