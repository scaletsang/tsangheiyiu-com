# What do you mean by "I am an artist"

[date] 13 May 2019
[tags] 
[connections] none

[abstract] Today I have been asked by Lynn, who are currently making documentary about art, a series of questions regarding basically what is art and why should we care: "Why does it matter to say that one is itself an artist?", "What do you have to differentiate between yourself as an artist and the public?", "in this sense, Isn't it egotistic to say that I am an artist?"

My immediate response was that saying I am an artist is a self-approval. I am bearing the risk of being view as egotistic or self-bluffing. But I did not answer the question directly. What do I even mean when I say I am an artist? 

For 80% of the answer, I would say I am a passionate creator. However, artist and creator is not equivalent, artist denotes a certain fluidity to its characteristics. I would not be bounded by the title of "creator". Instead, by saying I am an artist, I am asking myself all the time, where should I go next, how do I move on. There is no a specific position where I will be ever satisfy to be in, nor any ultimate goal. I am fluid.

And it is not about making a difference between myself and the public. I am just reminding myself that I have to aware of every changes of myself and the environment for the fact that I will never be the same.

"You mentioned I quite a number of times, shouldn't art be meant for making a better upon the public instead of merely an egotistical story about oneself?"

You are right, art always start with myself, me or I. Yet, it does not connotate that the artwork would be about me. Artwork is a pause between the start and the end. All artworks that one artist made convey the whole general line (or several lines) of thinking in his/her life. I am quite sure earlier works would be usually either imitation of other piece as well as rather egotistical, since we start from ourselves. As our thinking goes on, our thinking developed, our art pieces might have already moved away from ourselves, yet there is always an embedded link between the artwork and the artist inside of it.

Anyway, maybe being egotistical might allow me to find value in the art piece, then why not? However, consequences such as criticism and social disapproval could be foresee. If one could bear these consequences, and love being egotistical, I would encourage you to do it. Eventually, we all will die, if we do not satisfy ourselves when possible, it shall be a pity. 

"Shouldn't you spent your availability to benefit the public rather than producing egotistical piece?"

Chinese history told us that in order for a kingdom to develop their country, other than making logical statement in the policy, the king should also care about whether the policy is emotionally satisfied by the citizens. Other than logically telling ourselves that benefiting the public makes a better value in the art piece, one should also consider if the art piece satisfy myself emotionally as well. 

Back to the question, it is actually asking about the morality as an artist to produce beneficial ingredients for the public. I would say this morality is a choice as well. If the artwork that follow this morality satisfy me emotionally and logically, I do it. If not, then I would consider the consequences to go against this morality. If the consequences is unbearable no matter how hard I think, then I might just give up on this particular idea.

[Click here to download this writing](/media/all_writings/What_do_you_mean_by_I_am_an_artist.pdf)