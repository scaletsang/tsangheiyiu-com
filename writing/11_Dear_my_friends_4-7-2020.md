# Dear my friends

[date] 4 Jul 2020
[tags] 
[connections] none

Probably you didn't hear from me for weeks, for months or even for years. All the time that I choose not to connect with you isn't because I forgot about you, but I felt deeply naïve in a lot of things, I am trying to grow in a bit of isolation. I still need you, I still think of you, but most of you aren't near me at all. Part of the isolation is not intended.

When we are given lifes, we start to be influenced by the outer world, and we also start to cure some internal imbalance brought by the outer world. That is indeed a very chinese way of thinking. It is at this very moment I feel how accurate such description is. I have had ongoing diarrhea since 2 weeks ago. Each day I am thinking about what I should eat. Back in Hong Kong, I knew I would eat rice porridge when diarrhea, sometimes out of convenience, I could even order rice porridge from a restaurant. I could also cook soup with white gourd, or some other veggies that are kind to stomach. Here in Prague, most veggies I find in supermarkets are not that good for the stomach, and no one here sells rice porridge. I have to cook it by myself. Of course I learned to make rice porridge in several trials. This is an example of how a trivial matter leads up to dozens of problems. We were told to aim for higher intelligence, more knowledge in schools. How are they important when you do not know yourself well, when you are sitting on the toilet and cannot even think about anything else other than "what should I cook today?"

When we learn, we picture ourselves grabbing knowledge from outside and feeding it to ourselves. But shouldn't we start learning everything from ourselves? We might have missed the point all the time, maybe we learn so that we can see the biggest black box - ourselves. When I understand myself, not like dissecting myself into pieces and see what each part is responsible for, but like understanding how I should take care of myself, at that point I am able to do anything. But there's a catch, the process of understanding how to take care of ourselves probably lasts from birth to death. I don't really know when I am able to do anything, but I know it is not now, this isn't my time to do anything yet..

[abstract] I am not smart, because I am now even hesitating about what I should eat now. But I am thinking. And though I have friends who are nearby me, I couldn't think for them seriously, I am curing myself just like anyone in my age, but my body is more delicate, thus I need to pay extra care. The most I could do for each of my friends is my omnipresence, sometimes a glance, sometimes a message on whatsapp.

[Click here to download this writing](/media/all_writings/11_Dear_my_friends_4-7-2020.pdf)