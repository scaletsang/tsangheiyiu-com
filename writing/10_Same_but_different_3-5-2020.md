# Same but different

[date] 3 May 2020
[tags] 
[connections] none

## TAKE 1:

I have gotten no idea whatsoever for what I have done in the last few hours. I have upgraded my computer and found this virus that keeps spamming my computer until my computer runs out of memory. I have cooked myself a pot of rice mixed with previously cooked mushrooms. I have made a self surveillance session in which I exposed my living for 6 hours by setting up a CCTV in my room. I woke up, feeling refreshed. All these unrelated events, they are part of my archive. And in the archive, I compose, I edit, I distort. I am making sense out of nowhere, out of chaos, independent events.
Hummmmm... Ah ha!

Made a transcript of the day, found the connection between seperate things. Spotting out the virus in my life, realizing my own tendency to make certain choices.

Chop. chop. Chop. chop.

Cutting previously recorded footages, and composing them, seasoning them with care, trying to cook it differently each time, approaching it with heterogeneity.

Ughnhhh.

The moment I know I am being watched, I push myself to do something, to do something. I feel like I am connected but I am not, I am just being Anthony. Doing my own things, and sometimes sharing what I am doing. God knows what is privacy. I would tie my hair nicely and wear nice clothes for meeting my friends, but at the same time, wearing something less than pajamas might also present who I am without too much curation.

Woooahhhh! What time is it now?

I must be waking up late, crap, I have to zoom my teacher literally now, after 1 minute after I open my eyes.

What makes sense? Nothing makes sense, they just happen by themselves.

[abstract] How is your day? It’s fine. What do you expect me to answer, huh? Are you pissed? Not particularly, why? Sounds like you are. Oh, sometimes I am rude, sometimes I am nice, my time is not continuous. I change so much, today, tomorrow, last month in the Himalayas, today earlier skyping my old school friends. My day is not continuous, I can tell you how I could curate my day to tell you something interesting, but my day, everyday is a beautiful chaos on its own.

Cut.

------------------------------------------------------------------------

## TAKE 2:

Long forgot when I started to be obsessed with enjoying the chaos of this world. Today is no different than every day, at the same time today is so much different than every day. The superposition of hyperposition of states, the heterogeneity of approaching the same thing became the core of my current self. Trying to be fluid, gaining the ability to change myself, or change something outside of me, if needed, no matter what context I exist in. All just for necessity, and for fun. Greed is not a word in my dictionary, but suffice is.

Nice spoken, but guess what? The reality that I have to face is that I have to witness the chaos, listen to the unthinkable, bathing in the non-human centric ideas. They are not made to make sense of, but somehow my art is about making sense of this pile of crap, the good ones.

I spend so much time on the phone watching useless Youtube videos, it’s procrastination that is half good half sucky. The upside is that procrastination is necessary for creativity, but the downside is not so much from this form of procrastination. It is almost to the extent where I felt like watching those videos are to avenge the reality that I have to face the chaos, which i gotten myself into.

Recently, I found a good way of procrastination, cooking. Still, now I have developed an occupational disease of trying to do the same thing differently each time. As a result, uncooked rice is foreseeable, even with my Chinese origin. And what a surprise, today the mushroom rice is fucking PERFECTTTT. I think I found a mushroomlight within myself. Here is another occupational disease: Reading all the things that people recommended me to read, because I am also an irrelevant information collector. I do like to collect and read about irrelevant stuff outside of the things I like. And mushroomlight is from a polish youtube channel.

And what about your self-surveillance session? Would you think you can bullshit into your own practice? Emmm... I am afraid, I can’t. (Turn my head against the camera) Because I don’t want to now, I am too tired, can’t care enough.

Cut.

DIRECTOR: I think this take is a little bit too, humm, how should I say, too nihilistic, or almost negative (?)You have to I know it’s hard, but formalize yourself better, in a more presentable way.

------------------------------------------------------------------------

## TAKE 3:

I am for an art that is maximalistic. Complex systems and hyper-interconnectivity.

I am for an art that is buddhist. A calm, open-ended attitude towards the tragedy of seeing chaos.

No black, no white, no land, no borders, no high, no low, no human, no aliens, no singular, no plural. Language is delimiting, because we see things in oppositions, but not in more kinds of relationships. We should talk with uncertainty, with open-ended-ness, unlike this manifesto.

Cut.

Director: No manifesto in 2020! Cliche.

(Scene 1 faded out)

(DIrect cut to scene 2)

(Director got punched in the face)

Director: Ugh... (Trying to get up from his director chair) Who dafug hit me in my goddamm EYES?

(Scene 2 cut out) (Music)

Am I too care-free about being presentable whatsoever? I think I am completely care free. But since we are here, let me talk a bit. You must be curious about how that director is, but in fact he doesn’t exist right from the start. I am the director, I challenge myself, and I hit myself in my face. Sometimes you have to dis-continue to being yourself and see yourself as someone else, and sometimes give a good punch, well secretly. What I am doing is no different from the self surveillance session I did to myself, I am disclosing my actions, some of my thoughts, as well as sharing them to you, maybe you could get something out from here, but if not, that’s okay too, just like how I don’t always get interesting results from collecting irrelevant stuffs.

(Director punch me right on my face, classic film sequence, boring reverging stories)

Director: Now you are trying to deceive the audience to think that you are the smart guy but in fact you are just laying, slacking off in your own room all day long, huh?

(I am trying to talk on the floor, seeing duplicates of the director)

Aren....’t............yo...u........jus..t......a...li..ttl.e.....b...it.....too........har..s...h.....on.......m..e.?

Director: If not, why do you think I am here?

[Click here to download this script](/media/all_writings/10_Same_but_different_3-5-2020.pdf)

Egle invited me to read something or a poem I wrote. I wrote a 10 mins script before and this is the first time I read it out. Maybe I can make it a performance? Thanks Egle for the idea to make it a performance.

My first trial acting out this script without any preparation:
<iframe src="https://www.youtube.com/embed/38F5XAtXzd4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>