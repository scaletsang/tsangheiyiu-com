# Doing Nothing

[date] 3 Sept 2020
[tags] 
[connections] none

*This was my application to the scholarship for Doing Nothing 2020 hosted by Friedrich von Borries. Portal to the scholarship website: [link](https://www.hfbk-hamburg.de/en/projekte/schule-der-folgenlosigkeit/) It is supposed to be a grant application for initiating a certain project, but I just handed in the application for fun and for thinking.*

## What do you want not to do?

To read even a single book
 
## How long do you want not to do it for? 

2 months
 
## Why is it important not to do this particular thing?

[abstract] I love reading. It is not only a passion but also a career necessity because my brain is not big enough to make an artwork or design project, so I need to borrow brains from the past. However, I was diagnosed with a rare digestive disease recently, I could not absorb nutrition normally, thus losing 16 kgs in 3 months. There was a click in my mind that told me to alter my lifestyle, not only I have to choose what to eat, but I have to choose not to read.
 
It is about finding what is suitable for me, not what I am told of what might be good for me. I have to re-evaluate every part of my daily habits and surroundings. Restaurant food or I cook? High nutrition or low fat is more important? How can I go out when I do not even have enough energy to shop for 10 minutes? When in the day should I shop or cook so that I can distribute my limited energy to cater myself well even when I am sick? And then it comes to reading, is it suitable for me to read even though I still have a lot of free time left after doing all those errands? The answer seems to be that when I do absolutely nothing at the times that I usually would read, I experience boredom that triggers me to think about what really is important to myself. Turns out this chain of questions were produced during the times of my boredom.
 
Experiencing boredom to an intensity could trigger a sensitivity towards oneself and our environments. Boredom itself is painful, messy ideas and thoughts begin to pour in as I started to endure the boredom a bit. A bit more I would start questioning myself, about everything. It is in such a moment that I started to be observant of myself, just like seeing myself from someone else's perspective, I began to discover what I ignored and missed out on things that I usually do without thinking. Thanks to the boredom, I slowly become a bit more sensitive to my feelings in relation to my food intake, or the effects of the object placement in my room to my habits. Boredom triggers me to even be sensitive when I am not bored and allows me to be accurate of what actions, habits, foods are suitable for “the me” (human-specific) at this moment (context-specific).
 
## Why are you the right person not to do it?

I have reserved for myself at least 2 months of recovery for my body to an acceptable extent. I love books and I would encourage people to read. But in this specific situation, in this very body of mine, I need more boring times than usual to question and see what is suitable for every little part of my daily living now, thus I choose not to do the thing that I spent the most time on, and paused all my projects and university course/ activities. This is the choice of now, which means that after 2 months of recovery, what suits me to not do could be something else that triggers boredom, triggers thinking, this is a context-specific choice to not read. In another body, the same situation should be handled differently, to not read is a human-specific choice.
 
My digestive problem is not only my physical condition, but it is also the unnoticed condition of this society. My digestive disease did not come out of nowhere, it was the result of the accumulated fast food intake and my unawareness of what was going through my body. We have been constantly feeding ourselves fast food without noticing it. A faster way of achieving a Ph.D., more efficient designs to making food, more effective ways to receive knowledge. However, some detours from high school to Ph.D. might suits one's pace more; efficient food production might cause people to not care of their food because the food is easily delivered to your mouth and you have no responsibility to understand the food in order to eat it; sometimes learning online confuses you, because you are overloaded with various information, and do not know how to digestive this traffic. We are unaware that we are deceived to walk in a straight line, convenience, efficiency, and effectiveness are often about how things could be better implemented, but not how things could be implemented that suit the local environment (context-specific), and the communities associated with the things (human-specific).

In anyhow, I am now in no position to make big words about the society, because I am still ignorant of my body, I am still trying to put together the pieces I observe about my body while taking medication and work with my body. The body is one’s root of everything. Without a well-cared body, I do not see how I can be capable of caring about the knowledge I learn and care about how I use the knowledge! Then, how can I go on to care about others, and even support or take care of others? How can I furthermore care about the communities, and community space, or the environment that I live in?

No big words and no reading. Both encourage an attitude to leave space to do nothing and care/ take care of our very body that holds up all our intelligence and actions. Furthermore, to-not-read triggers boredom that reserves a bigger imaginary space to think for what suits me, to see what I should care about right now and how. 

//too long didn’t include// Doing nothing is a choice, a choice to create blanks on a Chinese traditional painting so that each stroke that you do is basic, fundamental for further development of the painting. Now the blank makes the strokes suitable for the painting, it is marvelous.

[Click here to download this writing](/media/all_writings/13_Doing_nothing_v3_3-9-2020.pdf)