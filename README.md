# Writing a blogpost

These are the self-defined keywords and its corresponding syntax when writing a markdown document for a blog post.

```markdown
[cover-img](/media/path-to-image)

[title] title

[date] date

[connections] none/name of another blogpost
* It is reminded that it is necessary to write none if there's no connection. The functionality of this tag will be implemented later when blogs need to connect to each other.

[abstract] abstract paragraph to be shown on the istope view of the blogposts, try to keep it short.

[tags] tag1, tag2, tag3

[presentation](/media/presentation-folder-name)
* The path can only be a folder containing slides
* The slides name contained in this folder has to be named as slide1, slide2, ... slide20 and so on.

[pdf](/media/pdf-name)
```

# Blogpost template:

```markdown

# Title

[cover-img](/media/cover_img.jpg)
[date] 8 Oct 2020 morning
[tags] ISP, curation, paper-sculpture
[connections] none

[abstract] abstract-paragraph


```