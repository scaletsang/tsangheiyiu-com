# Waffles Warrior 

[cover-img](/media/waffles_warrior/slide2.jpg)
[date] 11 Nov 2018
[tags] performance
[connections] none

[abstract] This was a performance piece called "Waffles Warrior" I did on 11 November 2018, Prague College Bishop's Court campus. It was about how I saw my vulnerability (weak immune system, 10 years lasting Ezcema) as a possibility to become armors that protect the great warrior, who is myself, to fight, develop, grow and survive with pride. 

The performance symbolically convey the process of having a vulnerability, cleaning up the mess produced from that vulnerability and using the mess to create an armor that protect us from the unforseeable danger and enlight us to continue our warrior's way.

[presentation](/media/waffles_warrior)

Trailor of the performance:
<iframe src="https://www.youtube.com/embed/GvW_dT7we3Q" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Documentation and reflections about this performance:
[pdf](/media/pdf/waffles_warrior.pdf)

A test run of the performance:
<iframe src="https://www.youtube.com/embed/nlMvMcuTaGY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Full documentation of the performance:
<iframe src="https://www.youtube.com/embed/CvClAauqzos" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>