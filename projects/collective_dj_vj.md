# Collective DJ/VJ (in-progress)

[cover-img](/media/collective_dj_vj_cover_img.jpg)
[date] 2 June 2020
[tags] installation
[connections] none

[abstract] This is an online installation using a livestream environment. Participants who joined the livestream can type whatever in the chat and influence real-time (still some inevitably delays but not too much) both the visuals and the sounds of the livestream. In effect, the participants are DJ-ing and VJ-ing collectively for themselves.

I am mostly responsible for making the digital architecture of the installation by wiring a Twitch bot that listens to the chat inputs from the audience, Ableton, Touch-designer, and OBS studio together. The sound and visuals are created by Alia Habib, a great Egyptian electronic musician and painter.

Gitlab link to the architecture of this digital installation:
<a href="https://gitlab.com/scaletsang/collective-dj-vj">https://gitlab.com/scaletsang/collective-dj-vj</a>

This is a test run of our digital installation.
<iframe src="https://www.youtube.com/embed/_UAyyu-LUxk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Documentation of the current process of the project:
[pdf](/media/pdf/collective_dj_vj.pdf)