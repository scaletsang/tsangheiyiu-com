# Boredom Project

[cover-img](/media/boredom_project/boredom_project_cover_img.jpeg)
[date] 1 June 2019
[tags] film, animation
[connections] none

[abstract] This is a project initiated our from my awareness to boredom. This marks the start of my on-going research on boredom, meaning, philosophy that relates to our current situation in modernized city. The project is the research itself, through experimenting ideas through actions, the research would be crystalized into products of experimental film, animation etc.

![img](/media/boredom_project/boredom_project01.jpeg)

The trailer of the experimental film produced from the research on boredom:
<iframe src="https://www.youtube.com/embed/PrzSItZRU_s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![img](/media/boredom_project/boredom_project02.jpeg)
![img](/media/boredom_project/boredom_project03.jpeg)
![img](/media/boredom_project/boredom_project04.jpeg)
![img](/media/boredom_project/boredom_project05.jpeg)
![img](/media/boredom_project/boredom_project06.jpeg)
![img](/media/boredom_project/boredom_project07.jpeg)
![img](/media/boredom_project/boredom_project08.jpeg)
![img](/media/boredom_project/boredom_project09.jpeg)

The full-length experimental film Human Oriented entity as a product of the research:
<iframe src="https://www.youtube.com/embed/vFlEmA_5DA8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![img](/media/boredom_project/boredom_project10.jpeg)

A Hand-drawn Animation created out of idea of optimistic nihilism, in which since no one is entitled to anything when we were born, when things happened, even though it is hard to accept somethings, maybe it is good to phrase it in a good way. Language have a power to us, if one monologue to himself/ herself over the same thing again and again, these monologue will change our mentality, framing one to think in a certain way. Therefore, phrasing some tragedy nicely could might make the day better.

The animation:
<iframe src="https://www.youtube.com/embed/Q0roF7EJcGI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>