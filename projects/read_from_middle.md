# A Rearrangable text

[cover-img](/media/my_wall.jpg)
[date] 20 Jan 2020 
[tags] book
[connections] none

Portal to this rearrangable text: 

<a href="http://read-from-middle.tsangheiyiu.com">http://read-from-middle.tsangheiyiu.com</a>

[abstract] This is a rearrangable text where the connections between the paragraphs are so strong that the paragraphs could be arranged in different order. Each time you re-fresh the webpage, you will get a different version of the same essay but with different order of the paragraphs. Also not all the paragraphs are used in each versions.

I have been recording my ideas and putting them in a database. I believed that new ideas do not come out of the blue, but they often come from our old or ignored ideas, hence the importance of archiving ideas. By finding more connections between ideas, one is able to produce creative ideas. How can I archive so that I could find more connections between ideas consequently produce new ideas? This media artifact is an attempt to experiment with one way to archive, one structure to archive.

I call this media artifact a rearrangeable text, for the reason that in the same pool of paragraphs, the paragraphs are arranged differently each time the reader refreshes the website that presents this rearrangeable text. I have created a big web of interconnected ideas from my extensive triangulation research about sensibility, improvisation, bias, etc. It is possible to write a proper academic essay that puts forth all these ideas to a focus, to a thesis statement. However, such a reductionist approach will not present the entirety of the whole web. This viewpoint is addressed clearly in Bohmian dialogue, it claims that reductionist thinking is powerful in understanding isolated things, which such understanding could help creating new technologies, however “its efficacy hinges on its being able to fragment or isolate its subject matter. It fails and may become actively dysfunctional when confronted by wholes, by the need to understand and take effective action in a highly interdependent context.” (Bohm, 2004, p.xii ) Consequently, what I do is I have a web of ideas, and I was writing the paragraphs as I travel along with the map. Each time I loop back to the point where I started, I stopped, and one arrangement is complete. Then I pick another starting point and do the same thing. As I begin to write the third and fourth loop, I write less and less, because ideas are interconnected and the paragraphs have a reusable property that allows themselves to be arranged differently. The end result is a website where every time it presents one arrangement of the web. When one refreshes the webpage, it would present a different arrangement. The reader would start seeing how ideas are connected differently despite reading from the same pool of paragraphs. It is an archive of what I have been reading for the past 4 months. By selectively having the juxtaposition of a few ideas at the same time, it creates a dynamic archive in which it may reveal different ways of looking at the ideas, leading to the capacity of creating more connections between existing ideas.

I think refreshing the website at least 4 times would allow one to understand most connections of ideas that are encapsulated and embedded in this text. Furthermore, the beauty of this text is that it is as fragile as music ought to be. Derek Bailey, in On the Edge: Improvisation in Music, revealed that European music was originally created with intentions to be improvised, the music was meant to be played, not to be preserved. (Bailey, 1992, Ep.1) This fragility of music is always the root of playing music, thus it is impossible to hear two exactly identical concerts. There is always a possible surprise, no matter how good or bad it is, music is always unique.

#### Citations:
- Bailey, D. (1992). On the Edge: Improvisation in Music Ep1. [Video] Available at: <a href="https://www.youtube.com/watch?v=edy2QlP_jaU&t=23s">https://www.youtube.com/watch?v=edy2QlP_jaU&t=23s</a> [Accessed 23 December 2019].
- Bohm, D. (2004). On Dialogue. New York, NY: Routledge.

This is a document of what I read and experienced in order to write this essay:
[pdf](/media/pdf/processbook.pdf)

## Development of the rearrangable text
Why am I refining my rearrangeable text?

23 October 2020

Reaching the end of summer, even though I was terribly sick, I was still messaging Olivier because he mentioned before that he wants to collaborate with me, so I have always considered him an intellectual who I could potentially collaborate with. When we talk, we always have a hype from some kind of misunderstanding between us, and then leading to some kind of mutual understanding. He does listen and make good questions. 

2 months ago, he asked if I have artworks to put in his exhibition in October/November that is about technology and human, I still don't quite get what it is about. I didn't really have anything that suits. Recently on 18th October 2020, for the first time in recent times, we called each other to talk about our interest in art, it was great. He brought up this exhibition again, and invited me to think if there's artwork I could put inside. I remembered Cristina recently told me that I built systems but did not think about the context that could place the system in. So there was a quick hologram of her at that moment that told me maybe it is a good chance to place my system in a context, and see how people react.

The piece is an essay consisting of 20 paragraphs, each paragraph could be read on its own, and the order of the paragraph could be changed and the sense the whole essay produces also changes. So I wrote an algorithm, everytime one refreshes the webpage, he will see one version of the essay, each version differs in the selection of paragraphs as well as the order in which the paragraphs are presented. It is an essay to show the possibility of ideas having different ways of connecting to each other, and that they produce different meanings by just having the ordering change. 

The flaw of this essay is that it is a dense essay, it requires attention and motivation to read and understand the point of the design. Olivier proposed to print booklets, each having one version of my essay printed on, then the visitors had to choose which one they would want to take. Further details would be one page one paragraph so the reader can have breaks between one idea and the idea. I think it is a good translation to put my essay into context.

I think Olivier is the first person who read it seriously and gave me feedback on this piece. He said that it would be better to give better titles to each essay (the title of the essay also changes according to the flow of the paragraphs) so the reader could faster get into the essay. A short introduction is also needed for giving textual context of why are you reading this essay now.

## DINN2 exhibition

Olivier van den Brandt invited me to materialize my essay physically in the exhibition he was holding. The essay was printed in 3 versions, totally about 50 copies. They were placed at the entrance of the exhibition and people can take it away if they like it.

This is the zine. Since the rearrangable text is now materialized as a zine, the zine is now called "Read from Middle" so to encourage people to read the zine from the middle of the book.
[pdf](/media/pdf/readfrommiddle.pdf)

The exhibition was showcased as part of the Eindhoven festival 2020 livestream.

Website: <a href="https://2020.eindhovenfestival.nl/watch/706b177b-b315-4725-90b3-18ded01462a1">https://2020.eindhovenfestival.nl/watch/706b177b-b315-4725-90b3-18ded01462a1</a>

This is the video of Olivier and Carlos Gonzales Moreno interviewing me regarding the Read from Middle zine.
<iframe src="https://www.youtube.com/embed/CNp3pE_4Ew8?start=118" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>