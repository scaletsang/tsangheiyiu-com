# Creating a Chinese programming environment (need collaborators)

[cover-img](/media/chinese_prog_lang.jpg)
[date] 2 June 2020
[tags] programming-environment
[connections] none

Portal to the gitlab page:
<a href="https://gitlab.com/scaletsang/pile-of-words">https://gitlab.com/scaletsang/pile-of-words</a>

[pdf](/media/pdf/chinese_prog_lang.pdf)