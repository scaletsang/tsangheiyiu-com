# Sensorship 

[cover-img](/media/sensorship_cover_img.jpg)
[date] 19 Jan 2020
[tags] performance
[connections] none

![img](/media/sensorship_cover_img.jpg)

[abstract] This is a performance where I was dancing to feel and build a relationship with a Leap motion sensor. It is just an absurd idea that I was imagining the sensor as a person that can also feels. The data received from the Leap motion sensor was live influencing the visual elements in the projections at the back.

Sensorship, full 10 minutes performance video documentation:
<iframe src="https://www.youtube.com/embed/OHVHd7LpLm4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Improvisation

I have been improvising a lot on the piano. Attended contemporary dance class for 3 months, practicing yoga. I am starting to feel a re-connection to my body, 9 months after Lisboa summer school that happened in May, 2019. I have been concentrating on my body, what kind of energy do I provoke? What can my body do? What is my body needing? All these questions started to submerged from the locked treasure chest, I feel a need to move, to use my body, to know my body, to be with my body. I used to focused too much on developing only “how can I think better”, but I realized it is impossible to think without a body.

I am at this point, still experimenting with my body, still experimenting with a project about finding a connection to my body via dancing with someone else. But the someone else would be a sensor.

Inspirations and process of preparing this performance:
![img](/media/sensorship1.jpg)
![img](/media/sensorship2.jpg)

Just me trying to feel my body through moving in my own room:
<iframe src="https://www.youtube.com/embed/AmQLa_4t2SA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The music played during the performance was a recorded piano improvisation:
<iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/track=393220630/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/transparent=true/" seamless><a href="https://anthonytsang.bandcamp.com/track/sensorship">Sensorship by Anthony Tsang</a></iframe>

Some other piano improvisation I did during 2019:
<iframe style="border: 0; width: 100%; height: 166px;" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/741295306&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
<iframe style="border: 0; width: 100%; height: 166px;" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/737153377&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
<iframe style="border: 0; width: 100%; height: 166px;" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/735770317&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
<iframe style="border: 0; width: 100%; height: 166px;" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/734894404&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
<iframe style="border: 0; width: 100%; height: 166px;" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/736259149&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
<iframe style="border: 0; width: 100%; height: 166px;" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/735329176&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
<iframe style="border: 0; width: 100%; height: 166px;" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/733359724&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>